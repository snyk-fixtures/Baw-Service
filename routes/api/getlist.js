var sql = require('../../config/dbtool');
var SqlString = require('sqlstring');
module.exports = function (req, res) {
  if(req.body.api_key && req.body.id){
    var api_key = req.body.api_key
    var id = req.body.id
    sql.query(SqlString.format('SELECT * FROM api1 WHERE owner=? and api_key=?', [id, api_key]), function(err, rows){
      if(err){ throw new Error('1번 질의 오류') }
      sql.query(SqlString.format('SELECT * FROM api2 WHERE owner=? and api_key=?', [id, api_key]), function(err, rows2){
        if(err){ throw new Error('2번 질의 오류') }
        var rcount = rows.length + rows2.length

        var no = 0
        var text = ""
        while ( no < rows.length ) {
          var text = text+api_key+";"+id+";"+rows[no]['cmd']
          if(no != rcount){
            var text = text+"'/"
          }
          no++
        }
        var no2 = 0
        while ( no2 < rows2.length ) {
          var text = text+api_key+";"+id+";"+rows2[no2]['cmd']
          if(no+no2 != rcount){
            var text = text+"'/"
          }
          no2++
        }
        res.send(text)
        sql.query(SqlString.format('delete from api1 WHERE owner=? and api_key=?', [id, api_key]))
        sql.query(SqlString.format('delete from api2 WHERE owner=? and api_key=?', [id, api_key]))

      })
    })
  } else {
    res.send("ERROR")
  }
};
