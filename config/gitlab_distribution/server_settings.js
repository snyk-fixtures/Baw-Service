module.exports = (function () {
  return {
    http_port: 80,
    https_port: 443,
    whitelist: true,
    hostname: "localhost",
    g_captcha_site_key: "6LeKTiwUAAAAANOYA7r37sTyquy_R0ljzSTVS6Bp",
    g_captcha_secret_key: "SECRET!",
    pretty_html: true,
    mail_sv: "smtp.worksmobile.com",
    mail_id: "a-mail-sender@rpgfarm.com",
    mail_pw: "SECRET",
    server_ip: "127.0.0.1",
    sms_id: "SECRET",
    sms_pw: "SECRET",
    sms_caller: "SECRET",
    tg_bot_key: "SECRET:SECRET",
    katalk_id: "SECRET",
    katalk_senderkey: "SECRET",
    freshchat_token: "disable",
    // freshchat_token: "SECRET",
    ad: false,
    sentry: 'sentry url',
    sentry_error: true,
    port_autobind: true
  }
})();
